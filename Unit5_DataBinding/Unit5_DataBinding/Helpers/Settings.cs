// Helpers/Settings.cs
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using Unit5.Core;

namespace Unit5_DataBinding
{
  /// <summary>
  /// This is the Settings static class that can be used in your Core solution or in any
  /// of your client applications. All settings are laid out the same exact way with getters
  /// and setters. 
  /// </summary>
  public static class Settings
  {
    private static ISettings AppSettings
    {
      get
      {
        return CrossSettings.Current;
      }
    }

    #region Setting Constants

    private const string SettingsKey = "settings_key";

        private const string IsRememnerKey = "remember_key";
    private static readonly string SettingsDefault = string.Empty;

    #endregion


    public static string GeneralSettings
    {
      get
      {
        return AppSettings.GetValueOrDefault<string>(SettingsKey, SettingsDefault);
      }
      set
      {
        AppSettings.AddOrUpdateValue<string>(SettingsKey, value);
      }
    }

        public static string Account
        {
            get { return AppSettings.GetValueOrDefault<string>("account_key", null); }
            set { AppSettings.AddOrUpdateValue<string>("account_key", value); }
        }


        public static bool isRemember
        {
            get { return AppSettings.GetValueOrDefault<bool>("remember_key", false); }
            set { AppSettings.AddOrUpdateValue<bool>("remember_key", value); }
        }

    }
}