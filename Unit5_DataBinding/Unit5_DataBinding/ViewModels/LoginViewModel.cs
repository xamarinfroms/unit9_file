﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using XamarinUniversity.Infrastructure;
using XamarinUniversity.Interfaces;
using XamarinUniversity.Services;

namespace Unit5_DataBinding
{
    public class LoginViewModel: INotifyPropertyChanged
    {
        public INavigationService Navigation { get; set; }

        public ICommand OnGoInfo { get; private set; }


     


        public event PropertyChangedEventHandler PropertyChanged;

        public LoginViewModel()
        {
            OnGoInfo = new Command(GoInfo);
        }

        public async void GoInfo()
        {
            await Navigation.NavigateAsync(
                AppPage.Info, 
                new InfoViewModel() { Navigation = this.Navigation });
        }


    }
}
