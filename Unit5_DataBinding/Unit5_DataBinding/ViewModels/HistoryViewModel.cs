﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unit5.Core;

namespace Unit5_DataBinding
{
    public class HistoryViewModel: INotifyPropertyChanged
    {
        public HistoryViewModel()
        {
            this.HistoryList = ProductFactory.GetHistory().Select(c => new ProductViewModel(c)).ToList();
        }

        public IList<ProductViewModel> HistoryList
        {
            get { return this._historyList; }
            set { this._historyList = value; this.PropertyChanged?.DynamicInvoke(new PropertyChangedEventArgs(nameof(this.HistoryList))); }
        }
        private IList<ProductViewModel> _historyList;

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
