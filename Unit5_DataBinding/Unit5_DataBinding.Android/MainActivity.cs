﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.IO;
using System.Collections.Generic;

namespace Unit5_DataBinding.Droid
{
    [Activity(Label = "Unit5_DataBinding", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            //{app目錄}/files
            var androidPathSystem= System.Environment
                .GetFolderPath(System.Environment.SpecialFolder.Personal);

            base.OnCreate(bundle);

           
            
            

                global::Xamarin.Forms.Forms.Init(this, bundle);
            LoadApplication(new App());
        }




}
}

